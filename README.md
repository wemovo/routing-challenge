# Search Bus Routes

### Problem
Implement a small REST API service which is able to return all the available bus routes containing the requested leg.

### Definitions
Data are provided as a text file. Each line represents a bus route in comma separated integers, described as it follows:
 - The first integer represents the **bus route identifier (bus_route_id)**, that is **unique** among all other bus route ids in the file.  
 - All the other integers represent a list of **station ids**.
 - A **leg** is a connection between two stations.
 - A station id may occur in multiple bus routes, but can **never** occur more than once within the **same** bus route.  


### Data Example
```
1,5,7,8,9
2,4,6,8
3,2,3,5,7
4,3,4,6,8
5,4,5,7,9
```

In the case of the first row, **1** is the ```bus_route_id``` and **5, 7, 8 and 9** are the station ids. **5** and **7** form a **Leg**.

### API

Your API should have a single URL that accept a GET request with two query parameters, ```origin``` and ```destination``` identifying station ids.  
Example: ```GET http://localhost:8080/api/routes?origin=5&destination=7```

### Expected Response
```
{
    "bus_routes_ids": [
        1,
        3,
        5
    ]
}
```

### Implementation
Please implement your solution in any language you are comfortable with.

### What we'll be evaluating
We expect you to demonstrate best practices for general software development. We will evaluate your source code as well as the functionality and compliance of the application.

###  Delivery
Once you're done send us your solutions via email or create a pull request.

###  Talk with us
We're more than happy to give you support and discuss any problem you have along the way. If you have questions don't hesitate in approaching us!